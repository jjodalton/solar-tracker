import math
import cmath
import time
from tkinter import *

def donothing():
    # do nothing
    time.sleep(1)

def _create_circle(self, x, y, r, **kwargs):
    return self.create_oval(x-r, y-r, x+r, y+r, **kwargs)
Canvas.create_circle = _create_circle

def _create_circle_arc(self, x, y, r, **kwargs):
    if "start" in kwargs and "end" in kwargs:
        kwargs["extent"] = kwargs["end"] - kwargs["start"]
        del kwargs["end"]
    return self.create_arc(x-r, y-r, x+r, y+r, **kwargs)
Canvas.create_circle_arc = _create_circle_arc


# Create the root window
root = Tk()


# Create the top frame
topFrame = LabelFrame(root, text='Installation Data')
topFrame.pack( side = TOP )

# Create the top canvas
topCanvas = Canvas(topFrame, bg="red", height=50, width=600)
topCanvas.pack()


# Create the mid frame
midFrame = Frame(root)
midFrame.pack()

# Create the right hand frame and add it to the mid frame
rightFrame = LabelFrame(midFrame, text='Elevation')
rightFrame.pack( side = RIGHT )

# Create the right hand canvas and add it to the right hand mid frame
rightCanvas = Canvas(rightFrame, bg="blue", height=300, width=300)
rightCanvas.pack()

# Create and arc segment in the canvas
rightCanvas.create_circle_arc(150, 200, 100, fill="green", outline="", start=0, end=180)
#rightCanvas.create_circle_arc(150, 200, 75, fill="blue", outline="", start=0, end=45)

# Create the left hand frame and add it to the mid frame
leftFrame = LabelFrame(midFrame, text='Azimuth')
leftFrame.pack( side = LEFT )

# Create the left hand canvas and add it to the left hand mid frame
leftCanvas = Canvas(leftFrame, bg="green", height=300, width=300)
leftCanvas.pack()

# Create a Circle in the canvas
leftCanvas.create_circle(150, 150, 100, fill="blue", outline="#DDD", width=4)
#leftCanvas.create_circle_arc(150, 150, 75, fill="green", outline="", start=0, end=45)


# Create the bottom frame
bottomFrame = LabelFrame(root, text='Status')
bottomFrame.pack()

# Create the top canvas
bottomCanvas = Canvas(bottomFrame, bg="white", height=50, width=600)
bottomCanvas.pack()


# Create the root menu bar
menubar = Menu(root)

# Create the items for the filemenu and add the menu to the bar
filemenu = Menu(menubar, tearoff=0)
filemenu.add_command(label="Test Display", command=donothing)
filemenu.add_separator()
filemenu.add_command(label="Exit", command=root.quit)
menubar.add_cascade(label="File", menu=filemenu)

# Create the items for the helpmenu and add the menu to the bar
helpmenu = Menu(menubar, tearoff=0)
helpmenu.add_command(label="About", command=donothing)
menubar.add_cascade(label="Help", menu=helpmenu)

# Add the menubar to the root window
root.config(menu=menubar)

root.mainloop()