#
#
# This is a quick test of the pySolar library set for the clifton observatory in bristol
#
#

# The goal of this is to calculate the information required to position a dual axis solar tracker throughout the day


import time
import datetime
import pysolar.solar

#
# This is fixed location based things
#
latitude_deg = 51.4572413
longitude_deg = -2.6125632
elevation_met = 79.3


d = datetime.datetime.utcnow()
thirty_minutes = datetime.timedelta(hours=0.5)

for i in range(48):
    timestamp = d.ctime()
    altitude_deg = pysolar.solar.get_altitude(latitude_deg, longitude_deg, d, elevation_met)
    azimuth_deg = pysolar.solar.get_azimuth(latitude_deg, longitude_deg, d, elevation_met)
    power = pysolar.solar.radiation.get_radiation_direct(d, altitude_deg)
    if (altitude_deg > 0):
        print(timestamp, "UTC", altitude_deg, azimuth_deg, power)
    d = d + thirty_minutes
    time.sleep(1)